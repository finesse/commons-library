package pw.finesse.commons.logging;

/**
 * A utility class containing static methods
 * allowing the user to print to console
 * in different formats.
 *
 * @author Marc
 * @since 9/4/2016
 */
public class Logger {

    /**
     * The formatting of the printed message.
     */
    private String format;

    public Logger(String format) {
        this.format = format;
    }

    public Logger() {
        this.format = "%s";
    }

    /**
     * Prints out the specified message.
     *
     * @param message message to print
     * @return printed message
     */
    public String print(String message) {
        // Creates a local instance of the formatted message.
        String formatted = String.format(this.format, message);

        // Prints the formatted message.
        System.out.println(formatted);

        return formatted;
    }

    /**
     * Prints out the specified message as an error.
     *
     * @param message message to print.
     * @return printed message
     */
    public String printError(String message) {
        // Creates a local instance of the formatted message.
        String formatted = String.format(this.format, message);

        // Prints the formatted message.
        System.err.println(formatted);

        return formatted;
    }
}
