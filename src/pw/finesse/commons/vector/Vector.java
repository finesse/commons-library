package pw.finesse.commons.vector;

/**
 * A utility class used for simple storage and simple access
 * of coordinates, meant to be used in a three or two dimensional
 * space.
 *
 * @author Marc
 * @since 9/10/2016
 */
public class Vector {

    /**
     * The x, y, and z coordinates of the vector.
     */
    private float x, y, z;

    public Vector(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Returns the distance to the specified vector.
     *
     * @param vector vector instance
     * @return distance to the specified vector
     */
    public float getDistance(Vector vector) {
        return this.getDistance(vector.getX(), vector.getY(), vector.getZ());
    }

    /**
     * Returns the distance to the specified coordinates.
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     * @return distance to the specified coordinates
     */
    public float getDistance(float x, float y, float z) {
        // Creates a local instance of the difference between coordinates.
        float difX = this.x - x;
        float difY = this.y - y;
        float difZ = this.z - z;

        return (float) this.getLength(difX, difY, difZ);
    }

    /**
     * Returns the length of the vector.
     *
     * @return length of the vector
     */
    public float getLength() {
        return this.getLength(this.x, this.y, this.z);
    }

    /**
     * Returns the length of the specified coordinates.
     *
     * @return length of the specified coordinates
     */
    public float getLength(float x, float y, float z) {
        return (float) Math.sqrt((x * x) + (y * y) + (z * z));
    }

    /**
     * Called to expand the vector.
     *
     * @param x x expansion value
     * @param y y expansion value
     * @param z z expansion value
     * @return expanded vector
     */
    public Vector expand(float x, float y, float z) {
        this.x += x;
        this.y += y;
        this.z += z;

        return this;
    }

    /**
     * Called to expand the vector.
     *
     * @param expansion expansion value
     * @return expanded vector
     */
    public Vector expand(float expansion) {
        this.expand(expansion, expansion, expansion);

        return this;
    }

    /**
     * Called to expand the vector.
     *
     * @param vector vector instance
     * @return expanded vector
     */
    public Vector expand(Vector vector) {
        this.expand(vector.getX(), vector.getY(), vector.getZ());

        return this;
    }

    /**
     * Called to contract the vector.
     *
     * @param x x contraction value
     * @param y y contraction value
     * @param z z contraction value
     * @return contracted vector
     */
    public Vector contract(float x, float y, float z) {
        this.x -= x;
        this.y -= y;
        this.z -= z;

        return this;
    }

    /**
     * Called to contract the vector.
     *
     * @param contraction contraction value
     * @return contracted vector
     */
    public Vector contract(float contraction) {
        this.contract(contraction, contraction, contraction);

        return this;
    }

    /**
     * Called to contract the vector.
     *
     * @param vector vector instance
     * @return contracted vector
     */
    public Vector contract(Vector vector) {
        this.contract(vector.getX(), vector.getY(), vector.getZ());

        return this;
    }

    /**
     * Called to multiply the vector.
     *
     * @param x x multiplication value
     * @param y y multiplication value
     * @param z z multiplication value
     * @return multiplied vector
     */
    public Vector multiply(float x, float y, float z) {
        this.x *= x;
        this.y *= y;
        this.z *= z;

        return this;
    }

    /**
     * Called to multiply the vector.
     *
     * @param multiplication multiplication value
     * @return multiplied vector
     */
    public Vector multiply(float multiplication) {
        this.multiply(multiplication, multiplication, multiplication);

        return this;
    }

    /**
     * Called to multiply the vector.
     *
     * @param vector vector instance
     * @return multiplied vector
     */
    public Vector multiply(Vector vector) {
        this.multiply(vector.getX(), vector.getY(), vector.getZ());

        return this;
    }

    /**
     * Called to divide the vector.
     *
     * @param x x division value
     * @param y y division value
     * @param z z division value
     * @return divided vector
     */
    public Vector divide(float x, float y, float z) {
        this.x /= x;
        this.y /= y;
        this.z /= z;

        return this;
    }

    /**
     * Called to divide the vector.
     *
     * @param division division value
     * @return divided vector
     */
    public Vector divide(float division) {
        this.divide(division, division, division);

        return this;
    }

    /**
     * Called to divide the vector.
     *
     * @param vector vector instance
     * @return divided vector
     */
    public Vector divide(Vector vector) {
        this.divide(vector.getX(), vector.getY(), vector.getZ());

        return this;
    }

    /**
     * Returns the x coordinate of the vector.
     *
     * @return x coordinate of the vector
     */
    public float getX() {
        return x;
    }

    /**
     * Returns the y coordinate of the vector.
     *
     * @return x coordinate of the vector
     */
    public float getY() {
        return y;
    }

    /**
     * Returns the z coordinate of the vector.
     *
     * @return z coordinate of the vector
     */
    public float getZ() {
        return z;
    }

    /**
     * Sets the x coordinate of the vector to the specified coordinate.
     *
     * @param x x coordinate to set
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * Sets the y coordinate of the vector to the specified coordinate.
     *
     * @param y y coordinate to set
     */
    public void setY(float y) {
        this.y = y;
    }

    /**
     * Sets the z coordinate of the vector to the specified coordinate.
     *
     * @param z z coordinate to set
     */
    public void setZ(float z) {
        this.z = z;
    }
}
