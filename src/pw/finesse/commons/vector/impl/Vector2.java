package pw.finesse.commons.vector.impl;

import pw.finesse.commons.vector.Vector;

/**
 * A utility class used for simple storage and simple access
 * of coordinates, meant to be used in a two dimensional
 * space.
 *
 * @author Marc
 * @since 9/11/2016
 */
public class Vector2 extends Vector {

    public Vector2(float x, float y) {
        super(x, y, 0);
    }
}
