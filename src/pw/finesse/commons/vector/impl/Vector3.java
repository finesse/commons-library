package pw.finesse.commons.vector.impl;

import pw.finesse.commons.vector.Vector;

/**
 * A utility class used for simple storage and simple access
 * of coordinates, meant to be used in a three dimensional
 * space.
 *
 *
 * @author Marc
 * @since 9/11/2016
 */
public class Vector3 extends Vector {

    public Vector3(float x, float y, float z) {
        super(x, y, z);
    }
}
