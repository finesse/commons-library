package pw.finesse.commons.timing;

import java.util.concurrent.TimeUnit;

/**
 * A class that allows for the user to
 * determine whether a specified time
 * has elapsed in a specified unit of
 * time.
 *
 * @author Marc
 * @since 8/2/2016
 */
public class Timer {

    /**
     * The last recorded system time in nano seconds.
     */
    private long lastTime = -1;

    /**
     * Return whether the specified time
     * has elapsed in milliseconds.
     *
     * @param time time elapsed
     * @return whether the time has elapsed
     */
    public boolean sleep(long time) {
        return this.sleep(time, TimeUnit.MILLISECONDS);
    }

    /**
     * Returns whether the specified time has elapsed
     * in the specified {@link TimeUnit}.
     *
     * @param time     time elapsed
     * @param timeUnit {@link TimeUnit} object
     * @return whether the time has elapsed
     */
    public boolean sleep(long time, TimeUnit timeUnit) {
        long conversion = timeUnit.convert(System.nanoTime() - lastTime, TimeUnit.NANOSECONDS);

        if (conversion >= time) {
            this.reset();
        }

        return conversion >= time;
    }

    /**
     * Updates the last time to the current system nano time.
     */
    public void reset() {
        this.lastTime = System.nanoTime();
    }
}
