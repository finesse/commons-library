package pw.finesse.commons.command;

import pw.finesse.commons.interfaces.Labelled;

/**
 * @author Marc
 * @since 8/11/2016
 */
public abstract class Command implements Labelled {

    /**
     * The label of the command.
     */
    private final String label;

    /**
     * The potential aliases of the command.
     */
    private final String[] aliases;

    public Command(String label, String... aliases) {
        this.label = label;
        this.aliases = aliases;
    }

    /**
     * Called when the command is ran and returns
     * the console output of the command.
     *
     * @param input input text
     * @return console output of the command
     */
    public abstract String run(String message, String... input);

    @Override
    public String getLabel() {
        return label;
    }

    /**
     * Returns the potential aliases of the command.
     *
     * @return potential aliases of the command
     */
    public String[] getAliases() {
        return aliases;
    }
}
