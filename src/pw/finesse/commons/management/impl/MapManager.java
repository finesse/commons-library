package pw.finesse.commons.management.impl;

import pw.finesse.commons.management.Manager;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * This class is an implementation of {@link Manager}
 * and contains methods useful for organization of
 * managers in need of a {@link Set}.
 *
 * @author Marc
 * @since 8/22/2016
 */
public class MapManager<K, V> implements Manager<V> {

    /**
     * The registry of elements.
     */
    protected Map<K, V> registry;

    public MapManager(Map<K, V> registry) {
        this.registry = registry;
    }

    /**
     * Registers the specified key and it's
     * corresponding value to the registry.
     *
     * @param key key to register
     * @param value value to register
     */
    public void register(K key, V value) {
        if(!this.registry.containsKey(key)) {
            this.registry.put(key, value);
        }
    }

    /**
     * Unregisters the specified key and it's
     * corresponding value from the registry.
     *
     * @param key key to unregister
     */
    public void unregister(K key) {
        if(this.registry.containsKey(key)) {
            this.registry.remove(key);
        }
    }

    /**
     * Returns {@code true} if the registry contains the specified key, otherwise {@code false}.
     *
     * @param key key to check
     * @return {@code true} if the registry contains the specified key, otherwise {@code false}
     */
    public boolean containsKey(K key) {
        return this.registry.containsKey(key);
    }

    /**
     * Returns {@code true} if the registry contains the specified value, otherwise {@code false}.
     *
     * @param value value to check
     * @return {@code true} if the registry contains the specified value, otherwise {@code false}
     */
    public boolean contains(V value) {
        return this.registry.containsValue(value);
    }

    /**
     * Returns the corresponding value to the
     * specified key from the registry.
     *
     * @param key key to get
     * @return corresponding value to the specified key from the registry
     */
    public V get(K key) {
        return this.registry.get(key);
    }

    /**
     * Returns the keys of the registry.
     *
     * @return keys of the registry
     */
    private Set<K> keySet() {
        return this.registry.keySet();
    }

    @Override
    public Collection<V> registry() {
        return this.registry.values();
    }

    @Override
    public int size() {
        return this.registry.size();
    }

    @Override
    public boolean isEmpty() {
        return this.registry.isEmpty();
    }

    @Override
    public void clear() {
        this.registry.clear();
    }
}
