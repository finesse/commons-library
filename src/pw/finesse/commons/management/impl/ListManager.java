package pw.finesse.commons.management.impl;

import pw.finesse.commons.management.Manager;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * This class is an implementation of {@link Manager}
 * and contains methods useful for organization of
 * managers in need of a {@link List}.
 *
 * @author Marc
 * @since 8/22/2016
 */
public class ListManager<E> implements Manager<E> {

    /**
     * The registry of elements.
     */
    protected List<E> registry;

    public ListManager(List<E> registry) {
        this.registry = registry;
    }

    /**
     * Registers or adds one or more specified elements into the registry.
     *
     * @param elements elements to register
     */
    public void register(E... elements) {
        Arrays.stream(elements).forEach(element -> this.registry.add(element));
    }

    /**
     * Unregisters or removes one or more specified elements from the registry.
     *
     * @param elements elements to unregister
     */
    public void unregister(E... elements) {
        Arrays.stream(elements).forEach(element -> this.registry.remove(element));
    }

    /**
     * Returns {@code true} if the registry contains the specified element, otherwise {@code false}.
     *
     * @param element element to check
     * @return {@code true} if the registry contains the specified element, otherwise {@code false}
     */
    public boolean contains(E element) {
        return this.registry.contains(element);
    }

    @Override
    public Collection<E> registry() {
        return this.registry;
    }

    @Override
    public int size() {
        return this.registry.size();
    }

    @Override
    public boolean isEmpty() {
        return this.registry.isEmpty();
    }

    @Override
    public void clear() {
        this.registry.clear();
    }
}
