package pw.finesse.commons.management;

import java.util.Collection;

/**
 * Implementations of this interface inherit
 * methods used for easier organization of
 * registries or managers.
 *
 * @author Marc
 * @since 8/15/2016
 */
public interface Manager<E> {

    /**
     * A collection of elements.
     *
     * @return collection of elements
     */
    Collection<E> registry();

    /**
     * Returns the size of the collection of elements.
     *
     * @return size of the collection of elements
     */
    int size();

    /**
     * Returns {@code true} if the collection is empty, otherwise {@code false}.
     *
     * @return {@code true} if the collection is empty, otherwise {@code false}
     */
    boolean isEmpty();

    /**
     * Clears the collection of all elements.
     */
    void clear();
}
