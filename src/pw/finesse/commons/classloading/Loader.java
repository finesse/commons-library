package pw.finesse.commons.classloading;

import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.Objects;

/**
 * An abstract class which accepts any type
 * of class to be loaded and execute actions
 * when done so.
 *
 * @author Marc
 * @since 8/2/2016
 */
public abstract class Loader<T> {
    /**
     * A generic class object of loaded class.
     */
    private Class<T> loadedClass;

    public Loader() {
        // Creates a local instance of the loaded class.
        Class<T> loadedClass = null;

        // The type representing the superclass of this class.
        Type superclass = this.getClass().getGenericSuperclass();

        // Checks if the superclass is a parameterized type.
        if (superclass instanceof ParameterizedTypeImpl) {
            // Casts the superclass to the parameterized type.
            ParameterizedTypeImpl parameterizedType = (ParameterizedTypeImpl) superclass;
            // Iterates through an array of type arguments.
            for (Type type : parameterizedType.getActualTypeArguments()) {
                // Checks if the type argument is a class.
                if (type instanceof Class) {
                    // Instantiates our loaded class instance with the correct type argument.
                    loadedClass = (Class<T>) type;

                    break;
                }
            }
        }

        // Checks if the loaded class is null before assigning it to a value.
        Objects.requireNonNull(this.loadedClass = loadedClass);
    }


    /**
     * The logic of the loaded class.
     *
     * @param loadedClass loaded class ran
     */
    public abstract void run(T loadedClass);

    /**
     * Called to instantiate the specified class
     * and return the instance of said class.
     *
     * @param clazz class to instantiate
     * @return class instance
     */
    public T invokeConstructor(Class<T> clazz) {
        // Iterates through the constructor(s) of the specified class.
        for (Constructor<?> constructor : clazz.getDeclaredConstructors()) {
            // If the constructor has 0 parameters.
            if (constructor.getParameterCount() == 0) {
                // Makes the constructor accessible.
                constructor.setAccessible(true);

                try {
                    // Returns a new instance of the constructor.
                    return (T) constructor.newInstance();
                } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    /**
     * Returns whether the class is properly
     * annotated and of the proper value.
     *
     * @param clazz loaded class instance
     * @return whether the class is valid
     */
    public boolean isClassValid(Class<?> clazz) {
        // Checks if the required annotation is present.
        if (clazz.isAnnotationPresent(AutoLoad.class)) {
            // Checks if the class of the annotation is of the proper class.
            if (clazz.getAnnotation(AutoLoad.class).value().equals(Loader.this.getLoadedClass())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the loaded class of the parameterized type.
     *
     * @return loaded class of the parameterized type
     */
    public Class<T> getLoadedClass() {
        return loadedClass;
    }
}
