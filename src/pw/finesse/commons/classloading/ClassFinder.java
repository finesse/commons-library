package pw.finesse.commons.classloading;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Stream;

/**
 * A utility class used to find classes
 * within specific packages.
 *
 * @author Marc
 * @since 8/4/2016
 */
public class ClassFinder {

    /**
     * The separator between packages.
     */
    private static final char PACKAGE_SEPARATOR = '.';

    /**
     * The separator between paths.
     */
    private static final char PATH_SEPARATOR = '/';

    /**
     * The suffix of a class file.
     */
    private static final String CLASS_FILE_SUFFIX = ".class";

    /**
     * Suppresses the default constructor, ensuring non-instantiability.
     */
    private ClassFinder() {
    }

    /**
     * Returns a collection of classes in the specified package.
     *
     * @param packageName package to check within
     * @param loader      {@link Loader} object
     * @return collection of classes in the specified package
     * @throws URISyntaxException in the case that the string cannot be parsed as a URI reference
     */
    public static List<Class<?>> find(Class sourceClass, String packageName, Loader loader) throws URISyntaxException {
        // Creates a local collection of classes.
        List<Class<?>> classes = new ArrayList<>();

        // Replaces the package separators with the proper path separators.
        String packagePath = packageName.replace(PACKAGE_SEPARATOR, PATH_SEPARATOR);

        // Finds the system resource corresponding to our code source.
        URL url = sourceClass.getProtectionDomain().getCodeSource().getLocation();

        // Creates a local instance of the path.
        Path path = Paths.get(url.toURI());

        // If the path is a directory, it means that it is being ran in an un-compiled environment.
        if (Files.isDirectory(path)) {
            // Finds the system resource corresponding to our package path.
            URL packageURL = ClassLoader.getSystemClassLoader().getResource(packagePath);

            // A file instance of the directory to check within.
            File directory = new File(packageURL.toURI());

            // Loads classes within the package path.
            ClassFinder.loadClasses(directory, classes, loader, packageName);
        } else {
            // Creates a local instance of the jar entries.
            try {
                Enumeration<JarEntry> entries = new JarFile(path.toFile()).entries();

                // Checks if the entries have more elements.
                while (entries.hasMoreElements()) {
                    // Creates a local instance of the current entry.
                    JarEntry entry = entries.nextElement();

                    if (entry.getName().endsWith(CLASS_FILE_SUFFIX)) {
                        ClassFinder.loadClasses(entry, classes, loader, packageName);
                    }
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        // Returns the collection of classes.
        return classes;
    }

    /**
     * Loads the classes in the specified directory and under the specified package path.
     *
     * @param directory   directory to initialize within
     * @param classes     list of classes to initialize
     * @param loader      {@link Loader} object
     * @param packageName package to initialize within
     */
    private static void loadClasses(File directory, List<Class<?>> classes, Loader loader, String packageName) {
        // Creates a local collection of the files located in the directory.
        File[] files = directory.listFiles();

        // Checks if the file collection is not null and longer than 0.
        if (files != null && files.length > 0) {
            // Iterates through the file collection.
            Stream.of(files).forEach(file -> {
                // Checks if the file is a directory.
                if (file.isDirectory()) {
                    // Re-run the method with a new package path.
                    ClassFinder.loadClasses(file, classes, loader, packageName + PACKAGE_SEPARATOR + file.getName());
                } else if (file.getName().endsWith(CLASS_FILE_SUFFIX)) {
                    // A new class instance of the file.
                    Class clazz = ClassFinder.loadClass(packageName, file);

                    // Checks if the class is valid to initialize.
                    if (loader.isClassValid(clazz)) {
                        // Adds the class instance to the collection of classes.
                        classes.add(clazz);
                    }
                }
            });
        }
    }

    /**
     * Loads the classes in the specified entry and under the specified package path.
     *
     * @param entry       entry to initialize from
     * @param classes     list of classes to initialize
     * @param loader      {@link Loader} object
     * @param packageName package to initialize within
     */
    private static <T> void loadClasses(JarEntry entry, List<Class<?>> classes, Loader loader, String packageName) throws ClassNotFoundException {
        // Creates a local instance of the entry's path.
        String path = entry.getName().replace(PATH_SEPARATOR, PACKAGE_SEPARATOR);

        // Checks if the entry's path matches.
        if (path.contains(packageName)) {
            Class<T> clazz = (Class<T>) ClassLoader.getSystemClassLoader().loadClass(path.substring(0, path.length() - CLASS_FILE_SUFFIX.length()));

            // Checks if the container is present and valid.
            if (clazz != null && loader.isClassValid(clazz)) {
                classes.add(clazz);
            }
        }
    }

    /**
     * Attempts to initialize the specified file in the
     * specified classpath. If the attempt fails,
     * the method will return {@code null}.
     *
     * @param classpath classpath to initialize from
     * @param file      {@link File} to initialize
     * @return loaded class
     */
    public static Class loadClass(String classpath, File file) {
        try {
            // Adds the necessary separators to the classpath.
            classpath = classpath + PACKAGE_SEPARATOR + file.getName();

            // Finds the class in the classpath and returns it.
            return Class.forName(classpath.substring(0, classpath.length() - CLASS_FILE_SUFFIX.length()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}