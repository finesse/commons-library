package pw.finesse.commons.classloading;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@code AutoLoad} annotation is used to mark classes
 * and point the {@link AutoLoader} to automatically initialize
 * the class at runtime.
 *
 * @author Marc
 * @since 8/4/2016
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AutoLoad {

    /**
     * The type of class to automatically initialize. This can be
     * used to filter out specific classes or types of classes
     * to segregate function.
     *
     * @return type of class to automatically initialize
     */
    Class<?> value();

}
