package pw.finesse.commons.classloading;

import java.net.URISyntaxException;

/**
 * A utility class which allows for the automatic loading of
 * classes of specific types in specific package paths. This
 * class is used in direct conjunction with the {@link AutoLoad}
 * annotation, which is used to mark specific classes in specific
 * package paths for automatic loading.
 *
 * @author Marc
 * @since 8/4/2016
 */
public class AutoLoader {

    /**
     * Suppresses the default constructor, ensuring non-instantiability.
     */
    private AutoLoader() {
    }

    /**
     * Called to initialize the classes in the specified package and
     * run the contents of the specified loader.
     *
     * @param sourceClass class loaded from
     * @param packageName package to check within
     * @param loader      loader object
     * @param <T>         type of object to initialize
     */
    public static <T> void load(Class sourceClass, String packageName, Loader<T> loader) {
        try {
            // Iterates through the classes in the specified package.
            ClassFinder.find(sourceClass, packageName, loader).forEach(clazz -> {
                // Instantiates the class.
                if (loader.isClassValid(clazz)) {
                    T element = loader.invokeConstructor((Class<T>) clazz);
                    // Checks if the element is not null.
                    if (element != null) {
                        // Runs the contents of the loader.
                        loader.run(element);
                    }
                }
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }


}