package pw.finesse.commons.randomization;

import java.security.SecureRandom;

/**
 * A utility class containing static methods
 * utilizing {@link SecureRandom} to generate
 * cryptographically strong truly random numbers.
 *
 * @author Marc
 * @since 8/16/2016
 */
public class Randomizer {

    /**
     * An instance of {@link SecureRandom}.
     */
    private static SecureRandom secureRandom;

    public Randomizer() {
        // Instantiates the instance.
        secureRandom = new SecureRandom();
    }

    /**
     * Returns a cryptographically strong random integer
     * between the specified floor and ceiling.
     *
     * @param floor   minimum value
     * @param ceiling maximum value
     * @return random number between the specified floor and ceiling
     */
    public int nextInt(int floor, int ceiling) {
        // The difference between the ceiling and the floor.
        int difference = ceiling - floor;

        return secureRandom.nextInt(difference + 1) + floor;
    }

    /**
     * Returns a cryptographically strong random integer
     * under the specified ceiling.
     *
     * @param ceiling maximum value
     * @return random integer under the specified ceiling
     */
    public int nextInt(int ceiling) {
        return secureRandom.nextInt(ceiling);
    }

    /**
     * Returns a cryptographically strong random integer.
     *
     * @return random integer
     */
    public int nextInt() {
        return secureRandom.nextInt();
    }

    /**
     * Returns a cryptographically strong random float.
     *
     * @return random float
     */
    public float nextFloat() {
        return secureRandom.nextFloat();
    }

    /**
     * Returns a cryptographically strong random double.
     *
     * @return random double
     */
    public double nextDouble() {
        return secureRandom.nextDouble();
    }

    /**
     * Returns a cryptographically strong random boolean.
     *
     * @return random boolean
     */
    public boolean nextBoolean() {
        return secureRandom.nextBoolean();
    }
}
