package pw.finesse.commons.property;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Properties marked by this annotation have
 * defined minimum and maximum values, and
 * can be clamped by the user at any time.
 *
 * @author Marc
 * @since 8/13/2016
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Clamp {

    /**
     * The minimum possible value of the property.
     *
     * @return minimum possible value of the property
     */
    double min();

    /**
     * The maximum possible value of the property.
     *
     * @return maximum possible value of the property
     */
    double max();
}
