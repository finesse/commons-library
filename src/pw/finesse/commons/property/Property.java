package pw.finesse.commons.property;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Fields marked by this annotation are
 * properties, able to be modified by
 * the user at any given time.
 *
 * @author Marc
 * @since 8/3/2016
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Property {

    /**
     * The label of the property. This is used
     * for graphical displays of the property.
     * The default value is a blank string,
     * since some properties don't require a
     * label.
     *
     * @return label of the property
     */
    String label() default "";

}
