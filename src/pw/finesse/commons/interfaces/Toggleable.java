package pw.finesse.commons.interfaces;

/**
 * Implementations of this interface inherit methods
 * allowing it to set a variable to either two different
 * states: true or false.
 *
 * @author Marc
 * @since 8/4/2016
 */
public interface Toggleable {

    /**
     * Returns {@code true} if the implementation is enabled, otherwise {@code false}.
     *
     * @return {@code true} if the implementation is enabled, otherwise {@code false}
     */
    boolean isEnabled();

    /**
     * Sets the enabled state of the implementation to the
     * specified enabled state. {@code true} will enable the
     * implementation, while {@code false} will disable the
     * implementation.
     *
     * @param enabled state to set
     */
    void setEnabled(boolean enabled);

    /**
     * Sets the enabled state of the implementation to
     * the opposite enabled state.
     */
    void toggle();
}
