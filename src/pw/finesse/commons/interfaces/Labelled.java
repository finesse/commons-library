package pw.finesse.commons.interfaces;

/**
 * Implementations of this interface
 * inherit a method returning a string.
 *
 * @author Marc
 * @since 8/2/2016
 */
public interface Labelled {

    /**
     * Returns the label of the implementation.
     *
     * @return label
     */
    String getLabel();
}
