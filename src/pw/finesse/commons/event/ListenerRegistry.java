package pw.finesse.commons.event;

import pw.finesse.commons.management.impl.SetManager;

import java.util.concurrent.CopyOnWriteArraySet;

/**
 * A registry containing all {@link Listener}s,
 * containing methods to fire events, register
 * and unregister listeners, and access the
 * registry. It is an implementation of the
 * {@link SetManager} interface, which allows
 * for better organization.
 *
 * @author Marc
 * @since 8/2/2016
 */
public class ListenerRegistry extends SetManager<Listener> {

    public ListenerRegistry() {
        super(new CopyOnWriteArraySet<>());
    }

    /**
     * Iterates through the registry of {@link Listener}s
     * to find the specified {@link Event}. If found, the specified
     * {@link Event} will be called and returned.
     *
     * @param event {@link Event} to fire
     * @param <T>   type of {@link Event to fire
     * @return {@link Event} instance
     */
    @SuppressWarnings("unchecked")
    public <T extends Event> T fire(T event) {
        // Iterates through the registry of Listeners.
        this.registry.forEach(listener -> {
            // Checks if the listener's event class equals the specified event class.
            if (listener.getEventClass() == event.getClass()) {
                // Runs the logic of the specified event.
                listener.run(event);
            }
        });

        return event;
    }
}
