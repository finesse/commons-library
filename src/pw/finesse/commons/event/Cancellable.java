package pw.finesse.commons.event;

/**
 * This class is an extension of {@link Event} and is a
 * cancellable variant. Extensions of this class are
 * able to be listened for and invoked at a specified
 * time. In addition, extensions of this class are
 * cancellable/stoppable.
 *
 * @author Marc
 * @since 8/2/2016
 */
public class Cancellable extends Event {

    /**
     * The cancelled state of the event.
     */
    private boolean cancelled;

    /**
     * Returns {@code true} if the event has been cancelled, otherwise {@code false}.
     *
     * @return {@code true} if the event has been cancelled, otherwise {@code false}
     */
    public boolean isCancelled() {
        return cancelled;
    }

    /**
     * Sets the cancelled state of the event to the specified cancelled
     * state. {@code true} cancels the event, while {@code false} allows
     * the event to continue running.
     *
     * @param cancelled state to set
     */
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    /**
     * Sets the cancelled state of the event to {@code true}, effectively
     * cancelling the event and preventing it from running.
     */
    public void cancel() {
        this.cancelled = true;
    }

}
