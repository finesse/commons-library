package pw.finesse.commons.event;

/**
 * This class is the root of the client
 * event bus hierarchy. Extensions of
 * this class are able to be listened
 * for and invoked at a specified time.
 *
 * @author Marc
 * @since 8/2/2016
 */
public abstract class Event {
}
