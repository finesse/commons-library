package pw.finesse.commons.event;

import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.Type;
import java.util.Objects;

/**
 * This abstract class is the generic
 * listener class, which accepts any
 * type of {@link Event} as a type to
 * listen for.
 *
 * @author Marc
 * @since 8/2/2016
 */
public abstract class Listener<T extends Event> {

    /**
     * A generic class object of the event.
     */
    private final Class<T> eventClass;

    public Listener() {
        // Creates a local instance of the event class.
        Class<T> eventClass = null;

        // The type representing the superclass of this class.
        Type superclass = this.getClass().getGenericSuperclass();

        // Checks if the superclass is a parameterized type.
        if (superclass instanceof ParameterizedTypeImpl) {
            // Casts the superclass to the parameterized type.
            ParameterizedTypeImpl parameterizedType = (ParameterizedTypeImpl) superclass;
            // Iterates through an array of type arguments.
            for (Type type : parameterizedType.getActualTypeArguments()) {
                // Checks if the type argument is a class.
                if (type instanceof Class) {
                    // Checks if the type argument is assignable from Event.
                    if (Event.class.isAssignableFrom((Class<?>) type)) {
                        // Instantiates our event class instance with the correct type argument.
                        eventClass = (Class<T>) type;

                        break;
                    }
                }
            }
        }

        // Checks if the event class is null before assigning it to a value.
        Objects.requireNonNull(this.eventClass = eventClass);
    }


    /**
     * The logic of the specified event.
     *
     * @param event event ran
     */
    public abstract void run(T event);

    /**
     * Returns the event class of the parameterized type.
     *
     * @return event class of the parameterized type
     */
    public Class<T> getEventClass() {
        return eventClass;
    }
}
