package pw.finesse.commons.parsing;

/**
 * A utility class that allows for the parsing of
 * a string into various other data types.
 *
 * @author Marc
 * @since 8/13/2016
 */
public class Parser {

    /**
     * Suppresses the default constructor, ensuring non-instantiability.
     */
    private Parser() {
    }

    /**
     * Parses a string from the specified class type, returning the parsed value.
     *
     * @param string string to parse
     * @param type   type of data
     * @return the parsed value
     */
    public static Object parse(String string, Class<?> type) {
        if (type == Byte.TYPE) {
            return Byte.parseByte(string);
        }

        if (type == Short.TYPE) {
            return Short.parseShort(string);
        }

        if (type == Integer.TYPE) {
            return Integer.parseInt(string);
        }

        if (type == Long.TYPE) {
            return Long.parseLong(string);
        }

        if (type == Float.TYPE) {
            return Float.parseFloat(string);
        }

        if (type == Double.TYPE) {
            return Double.parseDouble(string);
        }

        if (type == Boolean.TYPE) {
            return Boolean.parseBoolean(string);
        }

//        if(type.isEnum()) {
//            return (Object) Enum.valueOf(type, string);
//        }

        return null;
    }

}
